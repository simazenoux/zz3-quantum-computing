# Import Qiskit modules
from qiskit import QuantumCircuit, QuantumRegister, ClassicalRegister, execute

# Define 2 qubit and 2 classical registers
qr = QuantumRegister(2)
cr = ClassicalRegister(2)
circuit = QuantumCircuit(qr, cr)

# Apply Hadamard gate on both qubits
circuit.h(qr[0])
circuit.h(qr[1])

# Apply the Oracle operator
circuit.cx(qr[0], qr[1])
circuit.z(qr[1])

# Invert about average 
circuit.h(qr[0])
circuit.h(qr[1])
circuit.x(qr[0])
circuit.x(qr[1])
circuit.cz(qr[0], qr[1])
circuit.x(qr[0])
circuit.x(qr[1])
circuit.h(qr[0])
circuit.h(qr 
